var express = require('express');
var multer  = require('multer');
var upload = multer({ dest: './uploads/' });
var csv = require('csvtojson');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource 2');
});

router.post('/import', upload.single('file'), function(req, res, next) {
    var parsed = [];
    csv()
    .fromFile(req.file.path)
    .on('end_parsed', function(jsonObjects) {
        parsed = jsonObjects;
    })
    .on('done', function(error) {
        if (parsed.length > 0) {
            res.json(parsed);
        }
    });
    console.log(req.file);
});

module.exports = router;
